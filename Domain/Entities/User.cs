﻿using Domain.BaseEntity;
using Domain.Enum;

namespace Domain.Entities
{
    public class User : Base
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string Name { get; set; }
        public string ImageURL { get; set; }
        public StatusEnum Status { get; set; }
        public ICollection<Note>? Notes { get; set; }
    }
}
