﻿using Domain.BaseEntity;
using Domain.Enum;

namespace Domain.Entities
{
    public class Note : Base
    {
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public string Content { get; set; }
        public PriorityEnum Priority { get; set; }
        public NoteStatusEnum NoteStatus { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
