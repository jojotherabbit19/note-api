﻿namespace Domain.BaseEntity
{
    public abstract class Base
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public int? ModificatedBy { get; set; }
        public DateTime? DeletionDate { get; set; }
        public int? DeletedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
