﻿namespace Domain.Enum
{
    public enum StatusEnum
    {
        Active,
        Inactive,
        Banned
    }
}
