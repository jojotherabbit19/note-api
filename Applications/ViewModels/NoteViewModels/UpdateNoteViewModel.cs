﻿using Domain.Enum;

namespace Applications.ViewModels.NoteViewModels
{
    public class UpdateNoteViewModel
    {
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public string Content { get; set; }
        public PriorityEnum Priority { get; set; }
    }
}
