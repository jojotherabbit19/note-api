﻿namespace Applications.ViewModels.NoteViewModels
{
    public class NoteViewModel
    {
        public string Title { get; set; }
        public string Descriptions { get; set; }
        public string Content { get; set; }
    }
}
