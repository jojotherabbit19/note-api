﻿namespace Applications.ViewModels.UserViewModels
{
    public class UserViewModel
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string ImageURL { get; set; }
    }
}
