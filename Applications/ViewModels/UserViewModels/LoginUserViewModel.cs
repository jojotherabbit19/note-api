﻿namespace Applications.ViewModels.UserViewModels
{
    public class LoginUserViewModel
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
}
