﻿using Applications.Common;
using Applications.InterfaceServices;
using Applications.ViewModels.NoteViewModels;
using Domain.Entities;
using MapsterMapper;

namespace Applications.Services
{
    public class NoteService : INoteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTimeService _timeService;
        private readonly IClaimService _claimService;
        public NoteService(IUnitOfWork unitOfWork,
            IMapper mapper,
            ICurrentTimeService timeService,
            IClaimService claimService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _timeService = timeService;
            _claimService = claimService;
        }

        /// <summary>
        /// Add new note
        /// </summary>
        /// <param name="note"></param>
        /// <returns></returns>
        #region AddNote
        public async Task AddNote(CreateNoteViewModel note)
        {
            var noteObj = _mapper.Map<Note>(note);
            noteObj.UserId = _claimService.GetCurrentUserId;

            await _unitOfWork.NoteRepository.AddAsync(noteObj);
            await _unitOfWork.SaveChangeAsync();
        }
        #endregion

        /// <summary>
        /// Delete a note
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        #region SoftDeleteNote
        public Task SoftDeleteNote(int id)
        {
            throw new NotImplementedException();
        }
        #endregion

        /// <summary>
        /// Update a note
        /// </summary>
        /// <param name="note"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        #region UpdateNote
        public Task UpdateNote(UpdateNoteViewModel note)
        {
            throw new NotImplementedException();
        }
        #endregion

        /// <summary>
        /// View all note of current user
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>Pagination<Note></returns>
        #region ViewAllNote
        public async Task<Pagination<NoteViewModel>> ViewAllNote(int pageIndex = 0, int pageSize = 10)
        {
            var result = _mapper.Map<Pagination<NoteViewModel>>(await _unitOfWork.NoteRepository.GetUserNotesAsync());

            return result;
        }
        #endregion
    }
}
