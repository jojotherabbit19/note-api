﻿using Applications.Common;
using Applications.ViewModels.NoteViewModels;

namespace Applications.InterfaceServices
{
    public interface INoteService
    {
        Task AddNote(CreateNoteViewModel note);
        Task UpdateNote(UpdateNoteViewModel note);
        Task SoftDeleteNote(int id);
        Task<Pagination<NoteViewModel>> ViewAllNote(int pageIndex = 0, int pageSize = 10);
    }
}
