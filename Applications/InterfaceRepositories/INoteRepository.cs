﻿using Applications.Common;
using Domain.Entities;

namespace Applications.InterfaceRepositories
{
    public interface INoteRepository : IGenericRepository<Note>
    {
        Task<Pagination<Note>> GetUserNotesAsync(int pageIndex = 0, int pageSize = 10);
    }
}
