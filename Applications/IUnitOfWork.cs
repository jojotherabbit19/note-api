﻿using Applications.InterfaceRepositories;

namespace Applications
{
    public interface IUnitOfWork
    {
        public INoteRepository NoteRepository { get; }
        public IUserRepository UserRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
