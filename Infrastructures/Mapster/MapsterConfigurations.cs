﻿using Applications.ViewModels.NoteViewModels;
using Applications.ViewModels.UserViewModels;
using Domain.Entities;
using Mapster;

namespace Infrastructures.Mapster
{
    public class MapsterConfigurations : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            // create map for Note
            #region Map for Note
            config.NewConfig<CreateNoteViewModel, Note>();
            config.NewConfig<UpdateNoteViewModel, Note>();
            config.NewConfig<NoteViewModel, Note>();
            #endregion

            // create map for User
            #region Map for User
            config.NewConfig<LoginUserViewModel, User>();
            config.NewConfig<RegisterUserViewModel, User>();
            config.NewConfig<UpdateUserViewModel, User>();
            config.NewConfig<UserViewModel, User>();
            #endregion
        }
    }
}
