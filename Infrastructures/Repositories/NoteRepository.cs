﻿using Applications.Common;
using Applications.InterfaceRepositories;
using Applications.InterfaceServices;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class NoteRepository : GenericRepository<Note>, INoteRepository
    {
        private readonly AppDbContext _context;
        private readonly IClaimService _claimService;
        public NoteRepository(AppDbContext context,
            ICurrentTimeService currentTime,
            IClaimService claimService) :
            base(context, currentTime, claimService)
        {
            _context = context;
            _claimService = claimService;
        }

        /// <summary>
        /// Return Task<Pagination<Note>>
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        #region GetUserNotesAsync
        public async Task<Pagination<Note>> GetUserNotesAsync(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _context.Notes.Where(x => x.UserId == _claimService.GetCurrentUserId).CountAsync();
            var items = await _context.Notes.Where(x => x.UserId == _claimService.GetCurrentUserId)
                                            .AsNoTracking()
                                            .Skip(pageIndex * pageSize)
                                            .Take(pageSize)
                                            .AsNoTracking()
                                            .ToListAsync();

            var result = new Pagination<Note>()
            {
                TotalItemsCount = itemCount,
                PageIndex = pageIndex,
                PageSize = pageSize,
                Items = items
            };

            return result;
        }
        #endregion
    }
}
