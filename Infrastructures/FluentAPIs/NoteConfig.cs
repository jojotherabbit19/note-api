﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class NoteConfig : IEntityTypeConfiguration<Note>
    {
        public void Configure(EntityTypeBuilder<Note> builder)
        {
            builder.HasKey(x => x.Id);
            // config 1-M
            builder.HasOne(x => x.User)
                   .WithMany(x => x.Notes)
                   .HasForeignKey(x => x.UserId);
        }
    }
}
