﻿using APIs.Services;
using APIs.Validations;
using Applications.InterfaceServices;
using Applications.ViewModels.UserViewModels;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace APIs
{
    public static class DependencyInjection
    {
        public static IServiceCollection APIServices(this IServiceCollection services, IConfiguration configuration)
        {
            // support for claim
            {
                services.AddHttpContextAccessor();
                services.AddScoped<IClaimService, ClaimService>();
            }

            // defaul
            {
                services.AddControllers();
                services.AddEndpointsApiExplorer();
                services.AddSwaggerGen();
            }

            // validation and JWT
            {
                // server side
                services.AddFluentValidationAutoValidation();
                services.AddFluentValidationClientsideAdapters();
                // client side
                services.AddScoped<IValidator<LoginUserViewModel>, LoginValidation>();
                services.AddScoped<IValidator<RegisterUserViewModel>, RegisterValidation>();
                services.AddScoped<IValidator<UpdateUserViewModel>, UpdateUserValidation>();
                // JWT
                services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(o =>
                {
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = configuration["Jwt:Issuer"],
                        ValidAudience = configuration["Jwt:Audience"],
                        IssuerSigningKey = new SymmetricSecurityKey
                        (Encoding.UTF8.GetBytes(configuration["Jwt:Key"]!)),
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = false,
                        ValidateIssuerSigningKey = true
                    };
                });
                services.AddAuthorization();
            }

            return services;
        }
    }
}
