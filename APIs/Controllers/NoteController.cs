﻿using Applications.Common;
using Applications.InterfaceServices;
using Applications.ViewModels.NoteViewModels;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoteController : ControllerBase
    {
        private readonly INoteService _noteService;
        private readonly IMapper _mapper;
        public NoteController(INoteService noteService,
            IMapper mapper)
        {
            _noteService = noteService;
            _mapper = mapper;
        }

        //api: api/Note/GetUserNotes
        #region GetUserNotes
        [HttpGet("GetUserNotes")]
        [Authorize]
        public async Task<Pagination<NoteViewModel>> GetUserNotes(int pageIndex = 0, int pageSize = 10) => await _noteService.ViewAllNote(pageIndex, pageSize);
        #endregion

        //api: api/Note/AddNewNote
        #region AddNewNote
        [HttpPost("AddNewNote")]
        [Authorize]
        public async Task<IActionResult> AddNewNoteAsync(CreateNoteViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _noteService.AddNote(model);
                return Ok($"Add {model.Title} to my note.");
            }

            return BadRequest();
        }
        #endregion
    }
}
