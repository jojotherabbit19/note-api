﻿using Applications.InterfaceServices;
using Applications.ViewModels.UserViewModels;
using FluentValidation;
using MapsterMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly IValidator<LoginUserViewModel> _loginValidator;
        private readonly IValidator<RegisterUserViewModel> _registerValidator;
        private readonly IValidator<UpdateUserViewModel> _updateValidator;
        public UserController(IUserService userService,
            IMapper mapper,
            IValidator<LoginUserViewModel> loginValidator,
            IValidator<RegisterUserViewModel> registerValidator,
            IValidator<UpdateUserViewModel> updateValidator)
        {
            _userService = userService;
            _mapper = mapper;
            _loginValidator = loginValidator;
            _registerValidator = registerValidator;
            _updateValidator = updateValidator;
        }

        //api: api/User/Register
        #region Register
        [HttpPost("Register")]
        public async Task<IActionResult> Register(RegisterUserViewModel registerUser)
        {
            var result = _registerValidator.Validate(registerUser);
            if (result.IsValid)
            {
                var message = await _userService.RegisterAsync(registerUser);

                return Ok(message);
            }

            return BadRequest("Register Fail");
        }
        #endregion

        //api: api/User/Login
        #region Login
        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginUserViewModel loginUser)
        {
            var result = _loginValidator.Validate(loginUser);
            if (result.IsValid)
            {
                var token = await _userService.LoginAsync(loginUser);

                return Ok(token);
            }

            return BadRequest("Login Fail");
        }
        #endregion

        //api: api/User/GetAllUser
        #region GetAllUser
        [Authorize]
        [HttpGet("GetAllUser")]
        public async Task<List<UserViewModel>> GetAllUser()
        {
            var users = await _userService.GetUsersAsync();

            return _mapper.Map<List<UserViewModel>>(users);
        }
        #endregion
    }
}
