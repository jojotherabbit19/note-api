using APIs;
using Infrastructures;

var builder = WebApplication.CreateBuilder(args);
{
    builder.Services
        .APIServices(builder.Configuration)
        .InfrastructureServices(builder.Configuration);

}

var app = builder.Build();
{
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseAuthentication();
    app.UseHttpsRedirection();
    app.UseAuthorization();
    app.MapControllers();
    app.Run();
}
