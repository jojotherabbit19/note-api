﻿using Applications.ViewModels.NoteViewModels;
using AutoFixture;
using Domain.Entities;
using Domain.Test;
using FluentAssertions;

namespace Infrastructures.Test.MapperTests
{
    public class NoteMapperTest : SetupTest
    {
        [Fact]
        public void TestCreateNoteViewModelMapper()
        {
            //arrange
            var noteMock = _fixture.Build<Note>()
                                   .Without(x => x.User)
                                   .Create();

            //act
            var result = _mapperConfig.Map<CreateNoteViewModel>(noteMock);

            //assert
            result.Title.Should().Be(noteMock.Title);
        }

        [Fact]
        public void TestUpdateNoteViewModelMapper()
        {
            //arrange
            var noteMock = _fixture.Build<Note>()
                                   .Without(x => x.User)
                                   .Create();

            //act
            var result = _mapperConfig.Map<UpdateNoteViewModel>(noteMock);

            //assert
            result.Title.Should().Be(noteMock.Title);
        }

        [Fact]
        public void TestNoteViewModelMapper()
        {
            //arrange
            var noteMock = _fixture.Build<Note>()
                                   .Without(x => x.User)
                                   .Create();

            //act
            var result = _mapperConfig.Map<NoteViewModel>(noteMock);

            //assert
            result.Title.Should().Be(noteMock.Title);
        }
    }
}
