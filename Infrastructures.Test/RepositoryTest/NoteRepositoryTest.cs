﻿using Applications.InterfaceRepositories;
using AutoFixture;
using Domain.Entities;
using Domain.Test;
using FluentAssertions;
using Infrastructures.Repositories;

namespace Infrastructures.Test.RepositoryTest
{
    public class NoteRepositoryTest : SetupTest
    {
        private readonly INoteRepository _noteRepository;
        public NoteRepositoryTest()
        {
            _noteRepository = new NoteRepository(
                    _dbContext,
                    _currentTimeMock.Object,
                    _claimServiceMock.Object
                );
        }

        [Fact]
        public async Task NoteRepository_GetUserNotesAsync_ShouldReturnCorrectDataFirstsPage()
        {
            //arrange
            var mockData = _fixture.Build<Note>()
                                   .Without(x => x.User)
                                   .Without(x => x.UserId)
                                   .CreateMany(45)
                                   .ToList();
            await _dbContext.Notes.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            //act
            var paginasion = await _noteRepository.GetUserNotesAsync();

            //assert
            paginasion.PreviousPage.Should().BeFalse();
            paginasion.NextPage.Should().BeTrue();
            paginasion.Items.Count.Should().Be(10);
            paginasion.TotalItemsCount.Should().Be(45);
            paginasion.TotalPageCount.Should().Be(5);
            paginasion.PageIndex.Should().Be(0);
            paginasion.PageSize.Should().Be(10);
        }

        [Fact]
        public async Task NoteRepository_GetUserNotesAsync_ShouldReturnCorrectDataSecoundPage()
        {
            //arrange
            var mockData = _fixture.Build<Note>()
                                   .Without(x => x.User)
                                   .Without(x => x.UserId)
                                   .CreateMany(45)
                                   .ToList();
            await _dbContext.Notes.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            //act
            var paginasion = await _noteRepository.GetUserNotesAsync(1, 20);

            //assert
            paginasion.PreviousPage.Should().BeTrue();
            paginasion.NextPage.Should().BeTrue();
            paginasion.Items.Count.Should().Be(20);
            paginasion.TotalItemsCount.Should().Be(45);
            paginasion.TotalPageCount.Should().Be(3);
            paginasion.PageIndex.Should().Be(1);
            paginasion.PageSize.Should().Be(20);
        }

        [Fact]
        public async Task NoteRepository_GetUserNotesAsync_ShouldReturnCorrectDataLastPage()
        {
            //arrange
            var mockData = _fixture.Build<Note>()
                                   .Without(x => x.User)
                                   .Without(x => x.UserId)
                                   .CreateMany(45)
                                   .ToList();
            await _dbContext.Notes.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            //act
            var paginasion = await _noteRepository.GetUserNotesAsync(2, 20);

            //assert
            paginasion.PreviousPage.Should().BeTrue();
            paginasion.NextPage.Should().BeFalse();
            paginasion.Items.Count.Should().Be(5);
            paginasion.TotalItemsCount.Should().Be(45);
            paginasion.TotalPageCount.Should().Be(3);
            paginasion.PageIndex.Should().Be(2);
            paginasion.PageSize.Should().Be(20);
        }

        [Fact]
        public async Task NoteRepository_GetUserNotesAsync_ShouldReturnWithoutData()
        {
            //act
            var paginasion = await _noteRepository.GetUserNotesAsync();

            //assert
            paginasion.PreviousPage.Should().BeFalse();
            paginasion.NextPage.Should().BeFalse();
            paginasion.Items.Count.Should().Be(0);
            paginasion.TotalItemsCount.Should().Be(0);
            paginasion.TotalPageCount.Should().Be(0);
            paginasion.PageIndex.Should().Be(0);
            paginasion.PageSize.Should().Be(10);
        }
    }
}
