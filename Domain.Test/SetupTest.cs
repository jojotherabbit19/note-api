﻿using Applications;
using Applications.InterfaceRepositories;
using Applications.InterfaceServices;
using AutoFixture;
using Infrastructures;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using System.Reflection;

namespace Domain.Test
{
    public class SetupTest : IDisposable
    {
        protected readonly IMapper _mapperConfig;
        protected readonly Fixture _fixture;
        protected readonly Mock<IUnitOfWork> _unitOfWorkMock;
        protected readonly Mock<ICurrentTimeService> _currentTimeMock;
        protected readonly Mock<IClaimService> _claimServiceMock;
        protected readonly Mock<INoteService> _noteServiceMock;
        protected readonly Mock<IUserService> _userServiceMock;
        protected readonly Mock<INoteRepository> _noteRepositoryMock;
        protected readonly Mock<IUserRepository> _userRepositoryMock;
        protected readonly AppDbContext _dbContext;

        public SetupTest()
        {
            var config = TypeAdapterConfig.GlobalSettings;
            config.Scan(Assembly.GetExecutingAssembly());

            _mapperConfig = new Mapper(config);
            _fixture = new Fixture();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _currentTimeMock = new Mock<ICurrentTimeService>();
            _claimServiceMock = new Mock<IClaimService>();
            _noteServiceMock = new Mock<INoteService>();
            _userServiceMock = new Mock<IUserService>();
            _noteRepositoryMock = new Mock<INoteRepository>();
            _userRepositoryMock = new Mock<IUserRepository>();

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            _dbContext = new AppDbContext(options);

            _currentTimeMock.Setup(x => x.GetCurrentTime()).Returns(DateTime.UtcNow);
            _claimServiceMock.Setup(x => x.GetCurrentUserId).Returns(0);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
